import { LitElement, html } from "lit";
var app = "";
class MyElement extends LitElement {
  createRenderRoot() {
    return this;
  }
  render() {
    return html`
    <div data-theme="autumn" class="navbar bg-base-100">
  <div class="flex-1">
    <a href="/" class="btn btn-glass normal-case text-xl"><div class="font-title text-primary inline-flex text-lg transition-all duration-200 md:text-3xl"><span class="uppercase">B</span> <span class="text-base-content lowercase">Beauty</span></div></a>
  </div>
  <div class="flex-none gap-2">
    <div class="form-control">
    <input type="text" placeholder="Type here" class="input input-bordered input-info w-full max-w-xs" />
    </div>
    <div class="dropdown dropdown-end">
      <label tabindex="0" class="btn btn-ghost btn-circle avatar">
        <div class="w-10 rounded-full">
          <img src="https://images.unsplash.com/flagged/photo-1556743591-ca7e9c56d18d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=725&q=80" />
        </div>
      </label>
      <ul tabindex="0" class="mt-3 p-2 shadow menu menu-compact dropdown-content bg-base-100 rounded-box w-52">
        <li>
          <a class="justify-between">
            Profile
          </a>
        </li>
        <li><a>Settings</a></li>
        <li><a>Logout</a></li>
      </ul>
    </div>
  </div>
</div>

    <div data-theme="cupcake" class="hero min-h-screen" style="background-image: url(https://images.unsplash.com/photo-1493655161922-ef98929de9d8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80);">
  <div class="hero-overlay bg-opacity-60"></div>
  <div class="hero-content flex-col lg:flex-row-reverse">
  <img src="https://images.unsplash.com/photo-1684422296576-481b2e9810ed?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=387&q=80" class="max-w-xs rounded-2xl max-h-96 shadow-2xl" />
  <div>
  <div class="hero-content flex-col lg:flex-row-reverse">
  <img src="https://images.unsplash.com/photo-1620232828434-0b385fda6204?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=464&q=80" class="max-w-xs rounded-2xl max-h-96 shadow-2xl" />
  <div>
  <div class="hero-content flex-col lg:flex-row-reverse">
  <img src="https://images.unsplash.com/photo-1530785602389-07594beb8b73?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=387&q=80" class="max-w-xs rounded-2xl max-h-96 shadow-2xl" />
  <div>
    <h1 class="text-5xl font-bold decoration-red-900">Black Beauty</h1>
    <p class="py-6">Celebrating the exquisite allure of black-skinned girls. Explore a collection of stunning photographs and empowering stories that embody the essence of beauty, diversity, and strength.</p>
    <button onclick="window.location.href = '/'" class="btn btn-primary">Get Started</button>
  </div>
</div>
</div>
    `;
  }
}
window.customElements.define("my-element", MyElement);
export { MyElement };
